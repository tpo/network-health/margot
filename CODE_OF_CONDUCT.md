# Code of Conduct

The Tor Project is committed to fostering an inclusive community
where people feel safe to engage, share their points of view, and
participate. For the latest version of our Code of Conduct, please
see

https://gitlab.torproject.org/tpo/community/policies/-/blob/master/code_of_conduct.txt
