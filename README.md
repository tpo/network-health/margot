# margot

Margot is a Rust command line application using
[arti](https://gitlab.torproject.org/tpo/core/arti) that provides a series of
commands for the
[network health team](https://gitlab.torproject.org/tpo/network-health/team/).

## Current commands and subcommands

Note: part of this section should probably be moved into code documentation.

- `out-cfg`: `Create configuration entries`
  - `bad-exit [OPTIONS] <ticket_number> [query_args]` : `Generate bad exit rule(s)`
    for the DirAuths.

    The arguments are a ticket number (in `bad-relay-reports` project) and some
     `query_args`.

    The options are:
    -a, --added-by <ADDED_BY>  The person running this subcommand/adding the relays to the output [default: gk@torproject.org]
    -r, --reason <REASON>      The reason why adding these relays.
                                Possible `REASON`s:
                                0: Exit DNS failures.
                                1: Exit modified traffic.
                                2: Wrong family.
                                3: Protocol/version mismatch.
                                4: Same attacker as previously.
                                5: Possible sybil attack.
                                any other reason

    -p, --project <PROJECT>    The project name at gitlab.torproject.org/tpo/network-health/ to which the `ticket` belongs to [default: bad-relay-reports]

    A `query_arg` can be:
    - `addr:<IP address>`
    - `ff:<fingerprints file>`
    - `fl:<flag>`
    - `fp:<fingerprint>`
    - `p:<port>`
    - `v:<tor version>`
    A query_arg can be `exclude` (boolean), ie. not matching a filter, with the
    form `-:`, eg: `fl-:BADEXIT`

    Several `query_arg`s can be written one after another separated by a space.

    The output are the rules for `approved-routers.conf` in the form
    `!badexit <fp>`.

    If one of the `query_arg`s is `ff` or `fp`, `approved-routers.conf` will
    contain all the fingerprints, including those that weren't found in the
    consensus.
    If a fingerprint could not be parsed, it'll be ignored but printed to
    the stdout.

    Examples:
    - `out-cfg bad-exit 25`, output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      ----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit 000A10D43011EA4928A35F610405F92B4433B4DC
      [...]
      -----

      [+] Found 6810 relays: []
      ```

    - `out-cfg bad-exit 25 fl:BADEXIT`, output:

      ```bash
      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit 296B2178FD742AB35AB20C9ADF04D5DFD3D407EB
      [...]
      -----

      [+] Found 12 relays: [QueryArg { exclude: false, relay_attr: Flags(BAD EXIT) }]

      ```

    - `out-cfg bad-exit 25 fp:FFFBFB50A83A414CC21B4CDA93A9674B004705E8` , output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit FFFBFB50A83A414CC21B4CDA93A9674B004705E8
      -----

      [+] Found 1 relays: [QueryArg { exclude: false, relay_attr: Fingerprint(Rsa("FFFBFB50A83A414CC21B4CDA93A9674B004705E8")) }]

      ```

    - `out-cfg bad-exit 25 addr:24.203.134.20`, output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit FFFBFB50A83A414CC21B4CDA93A9674B004705E8
      -----

      [+] Found 1 relays: [QueryArg { exclude: false, relay_attr: Address(V4(Ipv4Network { addr: 24.203.134.20, prefix: 32 })) }]

      ```

    - `out-cfg bad-exit 25 addr:24.203.134.2`, output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      -----

      [+] Found 0 relays: [QueryArg { exclude: false, relay_attr: Address(V4(Ipv4Network { addr: 24.203.134.2, prefix: 32 })) }]

      ```

    - `out-cfg bad-exit 25 n:nestor00patof`, output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit FFFBFB50A83A414CC21B4CDA93A9674B004705E8
      -----

      [+] Found 1 relays: [QueryArg { exclude: false, relay_attr: Nickname("nestor00patof") }]

      ```

    - `out-cfg bad-exit 25 p:8888`, output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit 2BC31B73E0000B66981F7734D2B1F2C16C27D0BB
      !badexit 673510F48FA7EBE1C21A9A32566AB9B7AA8EFC48
      !badexit 94A8976E00C68ED23695D0668D87B3E7F126AF62
      -----

      [+] Found 3 relays: [QueryArg { exclude: false, relay_attr: Port(8888) }]

      ```

    - `out-cfg bad-exit 25 v:0.4.7.0`, output:

      ```bash
      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      -----

      [+] Found 0 relays: [QueryArg { exclude: false, relay_attr: Version("0.4.7.0") }]

      ```

    - `out-cfg bad-exit 25 fl:BADEXIT -p team`, output:
      ```bash
      [+] Rules for approved-routers.d/approved-routers.conf:

      -----

      # Ticket: https://gitlab.torproject.org/tpo/network-health/team/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !badexit 006F965E89A9C3A61C9F08A6B31C28F66AF218FD
      [...]
      -----

      [+] Found 93 relays: [QueryArg { exclude: false, relay_attr: Flags(RelayFlags(BAD_EXIT)) }]
      ```

    - `out-cfg bad-exit -r foo 25 fl:BADEXIT`
      ```bash
      [+] Rules for approved-routers.d/approved-routers.conf:

      -----

      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      # Reason: foo
      !badexit 006F965E89A9C3A61C9F08A6B31C28F66AF218FD
      [...]
      -----

      [+] Found 93 relays: [QueryArg { exclude: false, relay_attr: Flags(RelayFlags(BAD_EXIT)) }]
      ```

    - `out-cfg bad-exit -r 1 -a dgoulet 25 fl:BADEXIT`
      ```bash
      [+] Rules for approved-routers.d/approved-routers.conf:

      -----

      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: dgoulet@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      # Reason: Exit modified traffic.
      !badexit 006F965E89A9C3A61C9F08A6B31C28F66AF218FD
      [...]
      -----

      [+] Found 93 relays: [QueryArg { exclude: false, relay_attr: Flags(RelayFlags(BAD_EXIT)) }]
      ```

  - `reject`: `Generate reject rule(s)` for the DirAuths

    It works as the previous command but instead of generating rules for
    `approved-routers.conf` in the form `!badexit <fp>`, it generates rules
    like `!reject <fp>`.

    eg:
    - `out-cfg reject 25 p:8888`, output:

      ```bash

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !reject 2BC31B73E0000B66981F7734D2B1F2C16C27D0BB
      !reject 673510F48FA7EBE1C21A9A32566AB9B7AA8EFC48
      !reject 94A8976E00C68ED23695D0668D87B3E7F126AF62
      -----

      [+] Found 3 relays: [QueryArg { exclude: false, relay_attr: Port(8888) }]

      ```


  - `reject-bad <ticket_number> [query_args]` : `Generate reject rule(s), writing to bad.conf too`.

    It works as the previous command but it also generates rules for
    `bad.conf` in the form `AuthDirReject <ip>` plus comments with the
    corresponding relays' fingerprints.
    If any of the `query_arg`s is `ff` or `fp`, all the fingeprints will be
    written to `approved-routers.conf` as in the previous commands, but not
    to `bad.conf` when they can't be found in the current consensus.

    eg:

    - `out-cfg reject-bad 25 p:8888`, output:
      ```bash
      [+] Rules for torrc.d/bad.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      # Fingerprints:
      #               2BC31B73E0000B66981F7734D2B1F2C16C27D0BB
      #               5BE999DDB0916332AC21CE4AE9CED29FD7AAB284
      #               94A8976E00C68ED23695D0668D87B3E7F126AF62
      AuthDirReject 65.109.16.131
      AuthDirReject 104.200.30.152
      AuthDirReject 2600:3c03::f03c:93ff:fecc:2d20
      AuthDirReject 155.248.213.203
      -----

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !reject 2BC31B73E0000B66981F7734D2B1F2C16C27D0BB
      !reject 673510F48FA7EBE1C21A9A32566AB9B7AA8EFC48
      !reject 94A8976E00C68ED23695D0668D87B3E7F126AF62
      -----

      [+] Found 3 relays: [QueryArg { exclude: false, relay_attr: Port(8888) }]

      ```

    - `out-cfg reject-bad 25 ff:testdata/fps.txt`, output:
      ```bash
      Errors parsing testdata/fps.txt: Wrong fingerprint length: 0123456789abcdef0123456789abcdef0123456
      Errors parsing testdata/fps.txt: Wrong fingerprint length: 0123456789abcdef0123456789abcdef0123456
      [+] Rules for torrc.d/bad.conf:

      -----

      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      # Fingerprints:
      #               0011BD2485AD45D984EC4159C88FC066E5E3300E
      AuthDirReject 162.247.74.201
      -----

      [+] Rules for approved-routers.d/approved-routers.conf:

      -----

      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !reject 0011BD2485AD45D984EC4159C88FC066E5E3300E
      !reject 0123456789ABCDEF0123456789ABCDEF01234567
      -----

      [+] Found 1 relays: [QueryArg { exclude: false, relay_attr: FpsFileQuery([Rsa("0011bd2485ad45d984ec4159c88fc066e5e3300e"), Rsa("0123456789abcdef0123456789abcdef01234567")]) }]

      ```

  - `middle-only`: `Generate middleonly rule(s)` for the DirAuths.

    It works as the previous commands except that:
    - it does not generate rules for `bad.conf`.
    - instead of generating rules for `approved-routers.conf` like
      `!badexit <fp>`, it generates rules like `!middleonly <fp>`.

    eg:

    - `out-cfg middle-only 25 fl:middleonly`, output:

      ```bash
      [+] Rules for approved-routers.d/approved-routers.conf:

      -----
      # Ticket: https://gitlab.torproject.org/tpo/network-health/bad-relay-reports/-/issues/25
      # Added by: gk@torproject.org
      # Added at: 2023-10-31T11:14:07Z
      !middleonly 006F965E89A9C3A61C9F08A6B31C28F66AF218FD
      !middleonly 0082C49022C0811D45620D408E068835E2BABA71
      !middleonly 081A5BAF9775499CAF7CCCAB2AF7765494F3B99F
      ```

- `count [query_args]`: `Count relay(s) in the consensus`, optionally matching
  some `query_arg`.

  Filters are written as in the previous subcommands.

  eg:
  - `count fl:EXIT`, output:

     ```bash
     [+] 1603 relays match: QueryArg { exclude: false, relay_attr: Flags(EXIT) }
     [+] 1603 relays matched all
     ```

  - `count pp:"accept 80,443"`, output:

    ```bash
    [+] 199 relays match: QueryArg { exclude: false, relay_attr: PortPolicyAttr(PortPolicy { allowed: [PortRange { lo: 80, hi: 80 }, PortRange { lo: 443, hi: 443 }] }) }
    [+] 199 relays matched all
    ```

  - `pf:testdata/policy_accept.txt`, output:

    ```bash
    [+] 0 relays match: QueryArg { exclude: false, relay_attr: PortPolicyAttr(PortPolicy { allowed: [PortRange { lo: 20, hi: 23 }, PortRange { lo: 43, hi: 43 }, PortRange { lo: 53, hi: 53 }, PortRange { lo: 79, hi: 81 }, PortRange { lo: 88, hi: 88 }, PortRange { lo: 110, hi: 110 }, PortRange { lo: 143, hi: 143 }, PortRange { lo: 194, hi: 194 }, PortRange { lo: 220, hi: 220 }] }) }
    [+] 0 relays matched all
    ```

  - `count as:47172`, output

    ```bash
    [+] 4 relays match: exclude: false, relay_attr: Asn(47172)
    [+] 4 relays matched all
    ```

- `find`: `Find relay(s) in the consensus`, optionally matching some `query_arg`s.

  Filters are written as in the previous subcommands.

  Same for `--help`.

  eg:
  - `find n:nestor00patof`, output:

    ```bash
    [+] Nickname: nestor00patof
    > Fingerprint: Rsa: $FFFBFB50A83A414CC21B4CDA93A9674B004705E8, Ed: X+VJlS224jbNRejibbCPRKgVO8G64vC8S6nPSubfSpI
    > Flags: STABLE | RUNNING | VALID | V2DIR
    > Weight: Measured(45)
    > Version: 0.4.7.8
    > ORPort(s): 24.203.134.20:1337
    > IPv4 Policy: reject 1-65535
    > IPv6 Policy: reject 1-65535
    > Family:
    ```

- `like <name>`: `Match alike relay(s) in the consensus`.
   Compute relays nicknames' Levenshtein distance and print the
  `Top 5 closest nicknames to <name>`.

  eg:
  - `like named`, output:

    ```bash
    [+] Getting relays from consensus
    [+] Computing nickname distances...
    :: 1000 relays processed
    :: 2000 relays processed
    :: 3000 relays processed
    :: 4000 relays processed
    :: 5000 relays processed
    :: 6000 relays processed
    [+] Top 5 closest nicknames to: named
    -> $18ef781925edf5463338ab450ef0c56c5caa2f1a: 2
    -> $0e9cf92a1840341aecb39d1f9700c1baea51c284: 2
    -> $a0296ddc9ec50aa42ed9d477d51dd4607d7876d3: 2
    -> $0af90285d6b3ecc52abfa3cb31404df6faf93134: 2
    -> $4a69f9226b6113b0681d5c93bbd60d5fccbe4817: 2

    ```

- `sybil`: `Sybil testing`
  - `exitpolicy`: `Inspect Exit Policies`
    - Matching Reduced Exit Policy and More
    - Not matching Reduced Exit Policy

    eg:
    `sybil exitpolicy`, output:

    ```bash
    [+] Matching Reduced Exit Policy and More: 'accept 1-24,26-118,120-134,140-444,446-1213,1215-65535'
    +-----------+-------------------------------------------+---------------------------------------------+----------+---------------------+
    | Nickname  | Rsa                                       | Ed                                          | Version  | ORPorts             |
    +-----------+-------------------------------------------+---------------------------------------------+----------+---------------------+
    | SkyLights | $B0E93B10BD817250A818ABF7F5C2444AF364DD67 | oaLl3WmRC+4mKCNAvl4q+ghBwxkBvPgpcnAmP5NrABo | 0.4.7.10 | 173.237.206.68:9001 |
    +-----------+-------------------------------------------+---------------------------------------------+----------+---------------------+
    ```

- `sybilhunter <fingerprint>`: `Discover Sybil relays which are configured in a similar way`

    eg:
    `sybilhunter 501B3DBF250B094A05CA5DBC424AD4C3D46721A2`, output:

    ```bash
    Reference string: CalyxInstitute04162.247.74.20444374840.4.7.13075000
    [+] Computing distances...
    [+] Top 20 closest relays to: 501B3DBF250B094A05CA5DBC424AD4C3D46721A2
    distance: fingerprint, nickname, ip.port, flags bits, tor version, unmeasured measured
    0: 501B3DBF250B094A05CA5DBC424AD4C3D46721A2, CalyxInstitute04, 162.247.74.204443, 7484, 0.4.7.13, 075000
    [...]
    21: 93D3B5088A6813F679A426323AE0125FB4FE7728, RunningOnFumes1, 104.244.75.74443, 7484, 0.4.7.13, 06500
    ```

- `test`: `Run test(s) on one or many relay(s)`
  - `extend <query_args>`: Circuit `Extend to a relay`, optionally matching
    some `query_arg`s.

    Filters are written as in the previous subcommands.

    Possible outputs:
    - `Unable to extend: <relay>`
    - `Successful one hop to: <relay>`
    - `No relays matching query_args`

    eg:
    - `test extend fp:FFFBFB50A83A414CC21B4CDA93A9674B004705E8`, output:

      ```bash
      [+] Successful one hop to: nestor00patof - FFFBFB50A83A414CC21B4CDA93A9674B004705E8
      ```

    - `test extend fl:BADEXIT`, output:

      ```bash
      [..]
      [+] Successful one hop to: CGretski - 7ABA776A496C7B1D0C40F25ACA59F2EA60C3D429
      [-] Unable to extend: Problem building a circuit, while creating first hop with [87.120.37.231:9001 ed25519:qEHDLIxFvZ8FstcyXQY0gfqnpLVoEkjW8AkF64MdQAA $96733df529f50a69df592e4fcc116dc93832c91f]
      ```

- `versions`: `List the number of non-exits relays and exits by tor version and their consensus weight percentage`

  eg:
  `versions`, output:

   ```bash
   [+] Getting relays from consensus
      - 6833 running relays in consensus

    == Output format ==
    count: version   (MAJOR version) percent weight %

    == Relays (including exits) ==
       14: 0.4.5             [MAJOR] 0.00 %
        1: 0.4.5.12                  0.00 %

    [...]
       53: 0.4.8.1-alpha-dev         3.29 %

    === Sums ===
     2100:                           100.00 % (33 GB/s)

   ```

## Developing

[Rust](https://www.rust-lang.org/tools/install)

`cargo build run -- <command>`

## Online Relays module

`online_relays` is a Python module to generate "uptime" matrices similar to
[sybilhunter][] `uptime.go` module. See its README at `online_relays/README.md`

[sybilhunter]: https://github.com/NullHypothesis/sybilhunter

## License

This code is licensed under [MIT license](https://opensource.org/licenses/MIT).
Non-code files (documentation, configurations, images, etc.) are licensed
under [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/)
