
# Online Relays module

`online_relays` is a Python module to generate "uptime" matrices similar to
[sybilhunter][] `uptime.go` module.

It takes the data from the [metrics DB][] and generate CSV and the uptime/online
graphs matrix.

From the metrics DB, it needs the data from `hourly_relays_cw_flags` view at [network_status_churn_views.sql][] and `all_fingerprints` and `online_relays`
from [online_relays_views.sql]


Intermediate CSV data is generated in the `../data` directory.
The graphs are generated in the `../public` directory.

To visualize the graphs online, there is a different repository,
[online_relays][], which generates the graphs via Gitlab CI and pushes them to
the same repository. It then generates Gitlab Pages with the graphs at
[online_relays Pages][].

[sybilhunter]: https://github.com/NullHypothesis/sybilhunter
[online_relays]: https://gitlab.torproject.org/tpo/network-health/online_relays
[online_relays Pages]: tpo.pages.torproject.net/network-health/online_relays
[metrics DB]: https://gitlab.torproject.org/tpo/network-health/metrics/metrics-sql-tables
[network_status_churn_views.sql]: https://gitlab.torproject.org/tpo/network-health/metrics/metrics-sql-tables/-/blob/265e7f78d4e461b7e674a9967388af3b02e4bc64/views_functions/create/network_status_churn_views.sql#L89
[online_relays_views.sql]: https://gitlab.torproject.org/tpo/network-health/metrics/metrics-sql-tables/-/blob/ec399bd6bd598f962b363d1db3b001e71ddbe237/views_functions/create/online_relays_views.sql

## Installation in Debian/Ubuntu:

```bash
sudo apt install python3-poetry
poetry shell
cd online_relays
poetry install
```

Run it:
```bash
online_relays -h
```

