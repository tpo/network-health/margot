"""Plot relays online linkage matrix."""

# pylint: disable=C0114, C0115, C0116, C0209, W0212, W1202, W1203
import argparse
import datetime
import logging
import os.path
import pathlib
import sys
from collections import OrderedDict
from typing import Optional, Union

import numpy as np
import numpy.typing as npt
import pandas as pd
import plotly.graph_objects as go
import psycopg2
from dotenv import load_dotenv
from jinja2 import Template
from scipy.cluster.hierarchy import is_valid_linkage, leaves_list, linkage
from scipy.spatial.distance import pdist

PROJECT_PATH = pathlib.Path.cwd()
DATA_PATH = PROJECT_PATH / "data"
PUBLIC_PATH = PROJECT_PATH / "public"

SQL_RELAYS_ONLINE = """select *
from relays_online
where EXTRACT(MONTH FROM timestamp) = {}
order by timestamp, fingerprint;"""
DF_FPS_NAME = "fingerprint"
DF_DTS_NAME = "timestamp"
DF_VALUES_NAME = "online"

STATE_SQL = 1
STATE_RAW = 2
STATE_PRUNE = 3
STATE_DIST = 4
STATE_LINKAGE = 5
STATE_SORT = 6
STATE_HIGHLIGHT = 7
STATE_PLOT = 8

STATES_FNS = OrderedDict(
    {
        STATE_SQL: {
            "fn": "fetch_relays_month",
            "attr": "dffpsdtsonline",
            "file": "dffpsdtsonline.csv",
        },
        STATE_RAW: {
            "fn": "dffpsdtsonline2dffpsdts",
            "attr": "dffpsdtsonline",
            "file": "dffpsdtsonline.csv",
        },
        STATE_PRUNE: {
            "fn": "prune_dffpsdts",
            "attr": "dffpsdts_prune",
            "file": "dffpsdts_prune.csv",
        },
        STATE_DIST: {
            "fn": "dist_corr",
            "attr": "dffpsfps_dist_corr",
            "file": "dffpsfps_dist_corr.csv",
        },
        STATE_LINKAGE: {
            "fn": "linkage_matrix",
            "attr": "arrayfps_linkage",
            "file": "arrayfps_linkage.csv",
        },
        STATE_SORT: {
            "fn": "sort_matrix",
            "attr": "dffpsdts_sort",
            "file": "dffpsdts_sort.csv",
        },
        STATE_HIGHLIGHT: {
            "fn": "highlight_matrix",
            "attr": "dfdtsfps_highlight",
            "file": "dfdtsfps_highlight.csv",
        },
        STATE_PLOT: {
            "fn": "plot_dfdtsfps",
            # The plots directory is different and the following values will be
            # overwritten by dynamically.
            "attr": "graph",
            "file": "graph.svg",
        },
    }
)

DIST_CORR = 0
DIST_PDIST = 1
NOW = datetime.datetime.utcnow()
# Take the previous month, as current one hasn't finished.
MONTH_NUMBER = (NOW.replace(day=1) - datetime.timedelta(days=1)).month
# Take previous year if current month is January
YEAR = NOW.year if NOW.month != 1 else NOW.year - 1
# To change when we have more data
MONTHS = range(4, MONTH_NUMBER)
HTML_TEMPLATE = """<!DOCTYPE html>
<html>
  <head>
    <title>Relays Online matrix</title>
  </head>
  <body>
    <h1>Relays Online matrix</h1>
    <div class="container">
      {% block content %}
        <ul id="graphs">
        {% for graph in graphs %}
          <li>
            HTML: <a href="{{ graph.html }}">{{ graph.title }}</a>
            , SVG: <a href="{{ graph.svg }}">{{ graph.title }}</a>
          </li>
        {% endfor %}
        </ul>
      {% endblock %}
    </div>
  </body>
</html>"""

logging.basicConfig(
    level=logging.DEBUG,
)
logger = logging.getLogger(__name__)
load_dotenv()


def obtain_db_url() -> str:
    logger.debug("Obtaining URL to connect to DB.")
    if os.environ.get("DATABASE_URL", None):
        return os.environ["DATABASE_URL"]
    host: Optional[str] = os.environ.get("POSTGRES_HOST", None)
    port: Optional[str] = os.environ.get("POSTGRES_PORT", None)
    db: Optional[str] = os.environ.get("POSTGRES_DB", None)
    user: Optional[str] = os.environ.get("POSTGRES_USER", None)
    password: Optional[str] = os.environ.get("POSTGRES_PASSWORD", None)
    db_url: str = f"postgres://{user}:{password}@{host}:{port}/{db}"
    # Optional arguments
    sslmode: Optional[str] = os.environ.get("POSGTRES_SSLMODE", None)
    if sslmode:
        db_url += f"?sslmode={sslmode}"
        sslrootcert: Optional[str] = os.environ.get(
            "POSTGRES_SSLROOTCERT", None
        )
        db_url += f"&sslrootcert={sslrootcert}"
    return db_url


def connect_db() -> psycopg2.extensions.cursor:
    """Connect to the PostgreSQL database server."""
    db_url: Optional[str] = obtain_db_url()
    if not db_url:
        logger.error("No connection string")
        sys.exit(1)
    try:
        conn: psycopg2.extensions.connection = psycopg2.connect(db_url)
    # pylint: disable-next=broad-exception-caught
    except (Exception, psycopg2.DatabaseError) as error:
        logger.critical(error)
        sys.exit(1)
    conn.autocommit = True
    logger.info("Connected to the DB.")
    return conn.cursor()


def log_shape(matrix: Optional[pd.DataFrame | npt.NDArray]) -> None:
    if matrix is None:
        return
    rows, columns = matrix.shape
    logger.debug(f"{type(matrix)} with {rows} rows and {columns} columns.")


def csv2df(csvpath: pathlib.Path) -> pd.DataFrame:
    if "online" in csvpath.name or "fpsfps" in csvpath.name:
        df: pd.DataFrame = pd.read_csv(
            csvpath,
            parse_dates=True,
        )
        # Remove automatic indexes
        rows, columns = df.shape
        if columns == rows + 1:
            del df[df.columns[0]]
    elif "dffpsdts" in csvpath.name:
        df = pd.read_csv(
            csvpath,
            index_col=DF_FPS_NAME,
            parse_dates=True,
        )
        df.columns = df.columns.astype("datetime64[ns]")
        df.columns._name = DF_DTS_NAME  # type: ignore
    elif "dfdtsfps" in csvpath.name:
        df = pd.read_csv(
            csvpath,
            index_col=DF_DTS_NAME,
            parse_dates=True,
        )
        df.columns._name = DF_FPS_NAME  # type: ignore
    log_shape(df)
    return df


def is_df(matrix) -> bool:
    return isinstance(matrix, pd.DataFrame)


def is_ndarray(matrix) -> bool:
    return isinstance(matrix, np.ndarray)


def is_condensed(distmatrix) -> bool:
    """Check if the distance matrix is condensed."""
    return distmatrix.shape == (
        distmatrix.shape[0] * (distmatrix.shape[0] - 1) // 2,
    )


def is_redundant(distmatrix) -> bool:
    """Check if the distance matrix is redundant (`squareform`)."""
    return distmatrix.shape == (distmatrix.shape[0], distmatrix.shape[0])


class RelaysOnline:
    """Plot a Heatmap (matrix) with the relays online/offline for a month.

    0. Obtain a matrix with fps as rows and dts as column `dffpsdtsonline`
    1. Remove relays' time series always online `dffpsdts_prune`
    2. Calculate distance, clustering, order, highlight and plot
    2.1. Calculate Pearson’s correlation coefficients `dffpsfps_corr`
    2.2. Calculate distance between relays (fps) based on 2.1.
        `dffpsfps_dist_corr``
    2.1. Calculate distance using `pdist`
    2.2. `ndarrayfps_dist`
    2.3. Calculate single-linkage clustering `arrayfps_linkage`
    3. Sort sequences using single-linkage clustering `dffpsdts_sort`
    4. Annotate groups of x or more number of relays having the same sequence
       `dfdtsfps_highlight`
    5. Plot the matrix `fig`

    """

    def __init__(
        self,
        month_number=MONTH_NUMBER,
        dist_method=DIST_CORR,
        datapath=DATA_PATH,
        public_path=PUBLIC_PATH,
    ) -> None:
        self.cur: Optional[psycopg2.extensions.cursor] = None
        self.datapath: pathlib.Path = datapath
        self.dist_method: int = dist_method
        self.month_number: int = month_number
        self.year: int = YEAR
        self.month_filedirname = f"{self.year}{self.month_number:02d}"
        self.month_path: pathlib.Path = self.datapath / self.month_filedirname
        self.month_path.mkdir(parents=True, exist_ok=True)
        self.public_path: pathlib.Path = public_path
        self.public_path.mkdir(parents=True, exist_ok=True)
        self.state: int = 0
        self.dffpsdtsonline: Optional[pd.DataFrame] = None
        self.dffpsdts: Optional[pd.DataFrame] = None
        self.dffpsdts_prune: Optional[pd.DataFrame] = None
        self.ndarrayfps_dist: Optional[npt.NDArray] = None
        self.arrayfps_linkage: Optional[npt.NDArray] = None
        self.dffpsdts_sort: Optional[pd.DataFrame] = None
        self.dfdtsfps_highlight: Optional[pd.DataFrame] = None
        self.fig: Optional[go.fig] = None

    def run(self) -> None:
        self.state_from_file()
        while self.state < max(STATES_FNS.keys()):
            self.next_state()
            method = getattr(self, STATES_FNS[self.state]["fn"])
            method()

    def next_state(self) -> None:
        self.state += 1

    def state_from_file(self) -> None:
        for state in reversed(STATES_FNS.keys()):
            if state == STATE_PLOT:
                filepath = (
                    self.public_path
                    / f"{self.year}{self.month_number:02d}.html"
                )
            else:
                filepath = self.month_path / STATES_FNS[state]["file"]
            if os.path.isfile(filepath):
                self.state = state
                if state == STATE_PLOT:
                    logger.info(
                        "The graph for month %s already exists.",
                        self.month_number,
                    )
                    return
                df = csv2df(filepath)
                setattr(self, STATES_FNS[state]["attr"], df)
                return

    def df2csv(self) -> None:
        csvpath = self.month_path / STATES_FNS[self.state]["file"]
        df = getattr(self, STATES_FNS[self.state]["attr"], None)
        if df is None:
            return
        if is_df(df):
            df.to_csv(csvpath)
        elif is_ndarray(df):
            np.savetxt(csvpath, df, delimiter=",")

    def fetch_relays_month(self) -> None:
        """Fetch data from DB and create DataFrame with the form:

                  timestamp                               fingerprint  online
        2024-10-01 04:00:00  000A10D43011EA4928A35F610405F92B4433B4DC    True

        """
        self.cur = connect_db()
        query = SQL_RELAYS_ONLINE.format(self.month_number)
        logger.info("Fetching relays from DB, this could take a while....")
        self.cur.execute(query)
        rows = self.cur.fetchall()
        logger.info("Relays fetched from DB.")
        self.dffpsdtsonline = pd.DataFrame(
            rows, columns=[DF_DTS_NAME, DF_FPS_NAME, DF_VALUES_NAME]
        )
        log_shape(self.dffpsdtsonline)
        self.df2csv()
        self.cur.connection.close()

    def dffpsdtsonline2dffpsdts(self) -> None:
        logger.info(
            "Transforming datetime/fingerprint combinations to matrix."
        )
        if self.dffpsdtsonline is None:
            logger.error("No DataFrame.")
            return
        self.dffpsdts = self.dffpsdtsonline.pivot(
            index=DF_FPS_NAME, columns=DF_DTS_NAME, values=DF_VALUES_NAME
        )
        log_shape(self.dffpsdts)
        self.df2csv()

    def prune_dffpsdts(self) -> None:
        """Remove the fingerprints(rows) that have all their values equal to 1,
        ie. they're always online.
        """
        logger.info("Pruning dataframe.")
        if self.dffpsdts is None:
            logger.error("No DataFrame.")
            return
        mask: pd.Series = self.dffpsdts.apply(
            lambda column: column.nunique() == 1, axis=1
        )  # nfps
        self.dffpsdts_prune = self.dffpsdts.loc[~mask, :]
        log_shape(self.dffpsdts_prune)
        self.df2csv()

    def dist_corr(self) -> None:
        logger.info("Calculating distance matrix with pandas `corr`.")
        # To calculate fingerprints correlation coefficients, the dataframe
        # needs the fingerprints as columns
        if self.dffpsdts_prune is None:
            logger.error("No DataFrame.")
            return
        df_dts_fps: pd.DataFrame = self.dffpsdts_prune.transpose()
        dffpsfps_corr: pd.DataFrame = df_dts_fps.corr(method="pearson")
        log_shape(dffpsfps_corr)
        dffpsfps_dist_corr = 1 - np.abs(dffpsfps_corr)
        self.ndarrayfps_dist = dffpsfps_dist_corr.to_numpy()  # type: ignore
        log_shape(self.ndarrayfps_dist)
        logger.debug(
            "Distance matrix is condensed {}".format(
                is_condensed(self.ndarrayfps_dist)  # False
            )
        )
        self.df2csv()

    def dist_pdist(self) -> None:
        logger.info("Calculating distance matrix with numpy `pdist`.")
        if self.dffpsdts_prune is None:
            logger.error("No DataFrame.")
            return
        ndarrayfpsdts: npt.NDArray = self.dffpsdts_prune.to_numpy()
        # returns condensed distance matrix
        self.ndarrayfps_dist = pdist(
            ndarrayfpsdts, metric="correlation"
        )  # (19012861,)
        log_shape(self.ndarrayfps_dist)
        logger.debug(
            "Distance matrix is condensed {}".format(
                is_condensed(self.ndarrayfps_dist)  # True
            )
        )
        # The not condensed (squareform) form could be calculated, but in the
        # end the result should be similar
        self.df2csv()

    def dist_matrix(self) -> None:
        if self.dist_method == DIST_CORR:
            self.dist_corr()
        elif self.dist_method == DIST_PDIST:
            self.dist_pdist()

    def linkage_matrix(self) -> None:
        """Create the linkage matrix."""
        logger.info("Calculating linkage matrix.")
        self.arrayfps_linkage = linkage(
            self.ndarrayfps_dist, "single"
        )  # (nfps -1, 4)
        log_shape(self.arrayfps_linkage)
        logger.info(
            "Valid {} linkage matrix".format(
                is_valid_linkage(self.arrayfps_linkage)
            )
        )
        self.df2csv()

    def sort_matrix(self) -> None:
        if not is_valid_linkage(self.arrayfps_linkage):
            logger.error("Invalid linkage matrix.")
            return
        logger.info("Reordering matrix dataframe.")
        array_fps_sorted = leaves_list(
            self.arrayfps_linkage
        )  # len = nfingeprints
        if self.dffpsdts_prune is None:
            csvpath = self.month_path / STATES_FNS[STATE_PRUNE]["file"]
            self.dffpsdts_prune = csv2df(csvpath)
        fps_sorted = [
            self.dffpsdts_prune.index[row] for row in array_fps_sorted
        ]

        self.dffpsdts_sort = self.dffpsdts_prune.reindex(
            fps_sorted
        )  # nprunedfps, ndts
        log_shape(self.dffpsdts_sort)
        self.df2csv()

    def highlight_matrix(self, number_equals: int = 5) -> None:
        logger.info(f"Searching {number_equals} consecutive equal columns.")
        counter: int = 0
        equals: list = []
        if self.dffpsdts_sort is None:
            csvpath = self.month_path / STATES_FNS[STATE_SORT]["file"]
            self.dffpsdts_sort = csv2df(csvpath)
        # Replace True/False by 1/0
        # Avoid: FutureWarning:Downcasting behavior in `replace` is
        # deprecated...
        with pd.option_context("future.no_silent_downcasting", True):
            dffpsdts_sort = self.dffpsdts_sort.replace({True: 1, False: 0})
        dfdtsfps_highlight = dffpsdts_sort.transpose()
        for i in range(0, len(dfdtsfps_highlight.columns) - 1):
            j = i + 1
            if dfdtsfps_highlight[dfdtsfps_highlight.columns[i]].equals(
                dfdtsfps_highlight[dfdtsfps_highlight.columns[j]]
            ):
                # logger.debug(f"columns {i} and {j} are equals")
                counter += 1
                equals.append(dfdtsfps_highlight.columns[i])
                if counter > number_equals - 1:
                    # logger.debug(f"{counter} consecutive equal columns")
                    equals.append(dfdtsfps_highlight.columns[j])
                    # logger.debug(f"equals {equals}")
            else:
                if counter > number_equals - 1:
                    # logger.debug("storing equals")
                    for fp in equals:
                        dfdtsfps_highlight[fp] = dfdtsfps_highlight[
                            fp
                        ].replace({1: 0.5})
                # logger.debug("Resetting counters")
                counter = 0
                equals = []
        self.dfdtsfps_highlight = dfdtsfps_highlight
        log_shape(dfdtsfps_highlight)
        self.df2csv()

    def plot_dfdtsfps(self) -> None:
        logger.info("Drawing heatmap.")
        if self.dfdtsfps_highlight is None:
            logger.error("No dataframe.")
            return
        # plotly stills interpolates the colors even specifying zmin and zmax
        cmap: list = [
            [0, "white"],
            [0.2, "red"],
            [0.4, "red"],
            [0.6, "red"],
            [0.8, "red"],
            [1, "black"],
        ]
        figdata: go.Heatmap = go.Heatmap(
            z=self.dfdtsfps_highlight,
            x=self.dfdtsfps_highlight.columns,
            y=self.dfdtsfps_highlight.index,
            colorscale=cmap,
            showscale=False,
        )
        self.fig = go.Figure(data=figdata)
        self.fig.update_layout(
            title=f"Online relays during month {self.month_number}",
            xaxis_title="Fingerprints",
            yaxis_title="Consensuses",
            # Do not shop fingerprints in X axis
            xaxis_showticklabels=False,
        )
        extension: str = "html"
        figpath: pathlib.Path = (
            self.public_path / f"{self.month_filedirname}.{extension}"
        )
        extension = "svg"
        imgfigpath: pathlib.Path = (
            self.public_path / f"{self.month_filedirname}.{extension}"
        )
        self.fig.write_html(
            str(figpath),
            default_width=1280,
        )
        self.fig.write_image(
            str(imgfigpath),
            width=1280,
            height=800,
        )


class RelaysOnlineList:
    def __init__(self, public_path=PUBLIC_PATH) -> None:
        self.public_path: pathlib.Path = public_path
        self.graphs: list = []
        self.year: int = YEAR

    def obtain_graphs(self) -> None:
        for filepath in sorted(
            self.public_path.glob(f"{self.year}*.html"), reverse=True
        ):
            if os.path.isfile(filepath):
                graph: OrderedDict = OrderedDict(
                    {
                        "html": filepath.name,
                        "title": filepath.name.replace(".html", ""),
                        "svg": filepath.name.replace(".html", ".svg"),
                    }
                )
                self.graphs.append(graph)

    def add_graphs_to_index(self) -> None:
        html = Template(HTML_TEMPLATE).render({"graphs": self.graphs})
        with open(
            self.public_path / "index.html", "w", encoding="utf-8"
        ) as fd:
            fd.write(html)

    def run(self) -> None:
        self.obtain_graphs()
        self.add_graphs_to_index()


def create_parser() -> argparse.Namespace:
    """Create arguments parser."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__,
    )
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="Plot all months.",
    )
    parser.add_argument(
        "-m",
        "--month",
        type=int,
        help="Plot month.",
        default=MONTH_NUMBER,
    )
    parser.add_argument(
        "-d",
        "--dist-method",
        type=int,
        help=f"Distance method. {DIST_CORR} for correlation, "
        f"{DIST_PDIST} for pdist.",
        default=DIST_CORR,
    )
    parser.add_argument(
        "-i",
        "--only-index",
        action="store_true",
        help="Only generate index.html",
    )
    return parser.parse_args()


def main() -> None:
    args: argparse.Namespace = create_parser()
    if args.only_index:
        matrix_list = RelaysOnlineList()
        matrix_list.run()
        sys.exit(0)
    if args.all:
        months: Union[list | range] = MONTHS
    else:
        months = [args.month]
    for month in months:
        logger.info("Processing month %s", month)
        matrix = RelaysOnline(month, args.dist_method)
        matrix.run()
    matrix_list = RelaysOnlineList()
    matrix_list.run()


if __name__ == "__main__":
    main()
