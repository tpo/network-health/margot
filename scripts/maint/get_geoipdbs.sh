#!/bin/bash

set -x

prg_path=$0
if [ ! -e "$prg_path" ]; then
  case $prg_path in
    (*/*) exit 1;;
    (*) prg_path=$(command -v -- "$prg_path") || exit;;
  esac
fi
dir_prg_path=$(
  cd -P -- "$(dirname -- "$prg_path")" && pwd -P
) || exit

data_path=$(dirname -- $(dirname -- "$dir_prg_path"))/data

curl -o "$data_path"/geoip6-plus-asn https://tpo.pages.torproject.net/network-health/metrics/geoip-data/geoip6-plus-asn
curl -o "$data_path"/geoip-plus-asn  https://tpo.pages.torproject.net/network-health/metrics/geoip-data/geoip-plus-asn
