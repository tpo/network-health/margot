mod count;
mod err;
mod find;
mod like;
mod netop;
mod outcfg;
mod queries;
mod sybil;
mod sybilhunter;
mod util;
mod versions;

use anyhow::Result;
use async_trait::async_trait;
use clap::{Parser, Subcommand};

use tor_rtcompat::Runtime;

use crate::Runnable;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    pub subcmd: SubCmd,
    #[arg(short, long, default_value = "margot:info")]
    pub log_level: String,
}

#[derive(Subcommand, Debug)]
pub enum SubCmd {
    /// Create configuration entries from relay(s) matching a QueryExpr
    OutCfg(outcfg::OutCfgCmd),
    /// Count relay(s) in the consensus matching a QueryExpr
    Count(count::CountCmd),
    /// Find relay(s) in the consensus matching a QueryExpr
    Find(find::QueryExpr),
    /// Match alike relay(s) in the consensus
    Like(like::LikeCmd),
    /// Sybil testing
    Sybil(sybil::SybilCmd),
    /// Run tor network operations on one or many relay(s) matching a QueryExpr
    NetOp(netop::NetOpCmd),
    /// Discover Sybil relays which are configured in a similar way
    SybilHunter(sybilhunter::SybilHunterCmd),
    /// List the number of relays (including exits) and only exits by
    /// tor version and their consensus weight percentage
    Versions(versions::VersionsCmd),
}

impl SubCmd {
    fn cmd<R: Runtime>(&self) -> &(dyn Runnable<R> + Send + Sync) {
        match self {
            SubCmd::OutCfg(c) => c,
            SubCmd::Count(c) => c,
            SubCmd::Find(c) => c,
            SubCmd::Like(c) => c,
            SubCmd::Sybil(c) => c,
            SubCmd::NetOp(c) => c,
            SubCmd::SybilHunter(c) => c,
            SubCmd::Versions(c) => c,
        }
    }
}

#[async_trait]
impl<R: Runtime> Runnable<R> for SubCmd {
    async fn run(
        &self,
        arti_client: &arti_client::TorClient<R>,
    ) -> Result<()> {
        self.cmd().run(arti_client).await
    }
}
