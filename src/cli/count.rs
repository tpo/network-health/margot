use anyhow::Result;
use async_trait::async_trait;
use clap::Parser;
use std::fmt;

use crate::cli::queries;
use crate::cli::util;
use crate::runnable::RunnableOffline;

#[derive(Parser, Debug)]
pub struct CountCmd {
    #[arg(short, long)]
    list: bool,
    /// The query_args of this command.
    query_args: Vec<queries::QueryArg>,
}

impl fmt::Display for CountCmd {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.query_args)
    }
}

#[async_trait]
impl RunnableOffline for CountCmd {
    fn run(&self, netdir: &tor_netdir::NetDir) -> Result<()> {
        // We'll go query_arg by query_arg and then do a final count of all query_args.
        for query_arg in &self.query_args {
            let count =
                netdir.relays().filter(|r| query_arg.match_relay(r)).count();
            println!("[+] {} relays match: {}", count, query_arg);
            if self.list {
                let query_expr = queries::QueryExpr::new(&[query_arg.clone()]);
                let relays = query_expr.select_relays(netdir);
                util::describe_relays(&relays, true, 4);
            }
            // There can be only one unusable relay matching a rsa id
            let unusable_relay = query_arg.select_unusable_relay(netdir);
            if unusable_relay.is_some() {
                println!("[+] An unusable relay match {:?}", query_arg);
                if self.list {
                    println!("[+] {:?}", unusable_relay);
                }
            }
        }

        // Count relays with matching all query_args together.
        let query_expr = queries::QueryExpr::new(&self.query_args);
        println!("[+] {} relays matched all", query_expr.count(netdir));

        Ok(())
    }
}
