//! Declare an error type

use thiserror::Error;

use tor_netdoc::types::policy::PolicyError;

/// An error originated by a command.
#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid relay_attr: {0}")]
    InvalidFilter(String),
    #[error("Undecodable fingerprint: {0}")]
    UndecodableFingerprint(String),
    #[error("Unrecognized relay_attr: {0}")]
    UnrecognizedFilter(String),
    #[error("Wrong fingerprint length: {0}")]
    WrongFingerprintLength(String),
    #[error("Policy error: {0}")]
    WrongPolicy(#[from] PolicyError),
    #[error("IO error: {0}")]
    WrongIO(#[from] std::io::Error),
    #[error("Wrong parent: {0}")]
    WrongParent(String),
    #[error("No such relay")]
    NoSuchRelay,
}
