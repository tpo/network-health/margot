pub use crate::cli::queries::QueryExpr;
use crate::cli::util;
use crate::runnable::RunnableOffline;
use anyhow::Result;
use async_trait::async_trait;

#[async_trait]
impl RunnableOffline for QueryExpr {
    fn run(&self, netdir: &tor_netdir::NetDir) -> Result<()> {
        let relays = self.select_relays(netdir);
        if relays.is_empty() {
            println!("[-] No relays found");
            return Ok(());
        }
        util::describe_relays(&relays, self.oneline, 0);
        let unusable_relays = self.select_unusable_relays(netdir);
        if !unusable_relays.is_empty() {
            println!("[+] Found {} unusable relays", unusable_relays.len());
            for unsable_relay in unusable_relays {
                println!("[+] {:?}", unsable_relay)
            }
        }
        Ok(())
    }
}
