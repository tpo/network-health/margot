use anyhow::Result;
use async_trait::async_trait;
use clap::{Parser, Subcommand};
use std::fmt;
use std::ops::Deref;

use tor_rtcompat::Runtime;

use crate::cli::queries;
use crate::runnable::Runnable;

#[derive(Parser, Debug, Clone)]
pub struct ExtendCmd {
    /// The query_args of this command.
    query_args: Vec<queries::QueryArg>,
}

impl ExtendCmd {
    async fn extend<R: Runtime>(
        &self,
        arti_client: &arti_client::TorClient<R>,
    ) -> Result<()> {
        let mut found: bool = false;
        let query_expr = queries::QueryExpr::new(&self.query_args);
        let netdir = arti_client.dirmgr().timely_netdir().unwrap();
        let relays_iter =
            netdir.relays().filter(|r| query_expr.match_relay(r));

        for relay in relays_iter {
            found = true;
            // We take a copy of the fingerprint and nickname for later
            // printing because we loose ownership of the relay object once it
            // is in the TorPath.
            let fp =
                relay.rsa_id().to_string().replace('$', "").to_uppercase();
            let nickname = relay.rs().nickname().to_string();
            let circuit = arti_client.circmgr().deref();
            let one_hop_circ =
                circuit.get_or_launch_dir_specific(&relay).await;
            match one_hop_circ {
                Err(e) => println!("[-] Unable to extend: {}", e),
                Ok(_) => println!(
                    "[+] Successful one hop to: {} - {}",
                    nickname, fp
                ),
            };
        }
        if !found {
            println!(
                "[-] No relays matching query_args: {:?}",
                self.query_args
            );
        }
        Ok(())
    }
}

#[derive(Subcommand, Debug)]
pub enum NetOpSubCmd {
    /// Extend to a relay
    Extend(ExtendCmd),
}

#[derive(Parser, Debug)]
pub struct NetOpCmd {
    #[command(subcommand)]
    pub subcmd: NetOpSubCmd,
}

impl fmt::Display for NetOpCmd {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.subcmd)
    }
}

#[async_trait]
impl<R: Runtime> Runnable<R> for NetOpCmd {
    async fn run(
        &self,
        arti_client: &arti_client::TorClient<R>,
    ) -> Result<()> {
        match &self.subcmd {
            NetOpSubCmd::Extend(c) => c.extend(arti_client).await?,
        };
        Ok(())
    }
}
