use anyhow::Result;
use async_trait::async_trait;
use chrono::prelude::*;
use clap::{Parser, Subcommand};
use std::env;
use std::fmt;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::path::Path;
use std::str::FromStr;

use crate::cli::err::Error;
use crate::cli::queries;
use crate::runnable::RunnableOffline;

static DOMAIN: &str = "torproject.org";
static ADDED_BY: &str = "gk@torproject.org";
static TEAM_URL: &str = "https://gitlab.torproject.org/tpo/network-health/";
static ISSUES_URL_PATH: &str = "/-/issues/";

static REJECT_TOKENS: (&str, &str) = ("", "!reject");
static REJECTBAD_TOKENS: (&str, &str) = ("AuthDirReject", "!reject");
static BADEXIT_TOKENS: (&str, &str) = ("", "!badexit");
static MIDDLEONLY_TOKENS: (&str, &str) = ("", "!middleonly");

static APPROVED_ROUTERS_PATH: &str =
    "approved-routers.d/approved-routers.conf";
static BAD_PATH: &str = "torrc.d/bad.conf";

/// Reason why the relays are added to the output files via `out-cfg` .
#[derive(Debug, Clone, PartialEq)]
pub enum Reason {
    ExitDnsFailures,
    ExitTrafficModification,
    FamilyWrong,
    ProtoVersionMismatch,
    SameAsPrevious,
    SybilPossible,
    Other(String),
}

impl fmt::Display for Reason {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Reason::ExitDnsFailures => write!(f, "Exit DNS failures."),
            Reason::ProtoVersionMismatch => {
                write!(f, "Protocol/version mistmatch.")
            }
            Reason::FamilyWrong => write!(f, "Wrong family."),
            Reason::ExitTrafficModification => {
                write!(f, "Exit modified traffic.")
            }
            Reason::SameAsPrevious => {
                write!(f, "Same attacker as previously.")
            }
            Reason::SybilPossible => write!(f, "Possible sybil attack."),
            Reason::Other(s) => write!(f, "{}", s),
        }
    }
}

impl FromStr for Reason {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // There isn't a built-in way to do this from the enum discriminant.
        match s {
            "0" => Ok(Reason::ExitDnsFailures),
            "1" => Ok(Reason::ExitTrafficModification),
            "2" => Ok(Reason::FamilyWrong),
            "3" => Ok(Reason::ProtoVersionMismatch),
            "4" => Ok(Reason::SameAsPrevious),
            "5" => Ok(Reason::SybilPossible),
            &_ => Ok(Reason::Other(s.to_string())),
        }
    }
}

impl Reason {
    // Is there other easier/more elegant way to do this other than creating
    // each variant and displaying it?
    /// Help for possible reason values.
    pub fn long_help() -> String {
        "\
        The reason why adding these relays.\n \
        Possible `REASON`s:\n \
        0: Exit DNS failures.\n \
        1: Exit modified traffic.\n \
        2: Wrong family.\n \
        3: Protocol/version mistmatch.\n \
        4: Same attacker as previously.\n \
        5: Possible sybil attack.\n \
        any other reason\n \
        "
        .to_string()
    }
}

/// Return str followed by `@torproject.org` when the `str` doesn't contain `@`
///
/// Note that this is not an email validator.
pub fn added_by_parser(s: &str) -> Result<String> {
    if !s.contains('@') {
        Ok(format!("{s}@{}", DOMAIN))
    } else {
        Ok(s.to_string())
    }
}

/// Arguments for `out-cfg` subcommand.
///
/// This subcommand adds to the output files the current date/time as a
/// comment, though it doesn't have an option to add it by arguments.
///
#[derive(Parser, Debug, Clone)]
pub struct OutCfgSubCmdArgs {
    /// The person running this subcommand/adding the relays to the output.
    #[arg(short, long, default_value = ADDED_BY, value_parser = added_by_parser)]
    added_by: String,
    #[arg(short, long, long_help = Reason::long_help())]
    reason: Option<Reason>,
    /// The project name at gitlab.torproject.org/tpo/network-health/
    /// to which the `ticket` belongs to.
    #[arg(short, long, default_value = "bad-relay-reports")]
    project: String,
    /// The issue number in the project.
    ticket: u32,
    /// The query_args of this command.
    query_args: Vec<queries::QueryArg>,
}

#[derive(Subcommand, Debug)]
pub enum OutCfgSubCmd {
    /// Generate bad exit rule(s)
    BadExit(OutCfgSubCmdArgs),
    /// Generate reject rule(s)
    Reject(OutCfgSubCmdArgs),
    /// Generate reject rule(s), writing to bad.conf too
    RejectBad(OutCfgSubCmdArgs),
    /// Generate middleonly rule(s)
    MiddleOnly(OutCfgSubCmdArgs),
}

#[derive(Parser, Debug)]
pub struct OutCfgCmd {
    #[command(subcommand)]
    pub subcmd: OutCfgSubCmd,
}

fn fmt_addr_rule(prefix: &str, relay: &tor_netdir::Relay<'_>) -> String {
    let rules: Vec<_> = relay
        .rs()
        // This returns all the ORPort IPs (v4 and v6)
        .orport_addrs()
        .map(|a| {
            if a.is_ipv4() {
                format!("{} {}", prefix, a.ip())
            } else {
                // Enclose v6 IPs in brackets
                format!("{} [{}]", prefix, a.ip())
            }
        })
        .collect();
    [rules.join("\n"), "\n".to_string()].concat()
}

fn fmt_fp_rule(prefix: &str, relay: &tor_netdir::Relay<'_>) -> String {
    format!(
        "{} {}\n",
        prefix,
        relay.rsa_id().to_string().replace('$', "").to_uppercase()
    )
}

fn fmt_fp_str_rule(prefix: &str, fp: &str) -> String {
    format!("{} {}\n", prefix, fp)
}

impl fmt::Display for OutCfgCmd {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.subcmd)
    }
}

impl OutCfgSubCmdArgs {
    fn comment(&self) -> String {
        // Add new line at the start of the comment to visually separated from
        // previous rules
        let url = [
            TEAM_URL,
            &self.project,
            ISSUES_URL_PATH,
            &self.ticket.to_string(),
        ]
        .concat();
        let comments = format!(
            "\n\
            # Ticket: {url}\n\
            # Added by: {}\n\
            # Added at: {}\n",
            self.added_by,
            // To do not print subseconds nor timezone (it's UTC)
            Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true),
        );
        // Is there a more elegant way to do this?
        if self.reason.is_some() {
            format!(
                "{}# Reason: {}\n",
                comments,
                self.reason.as_ref().unwrap()
            )
        } else {
            comments
        }
    }

    fn print_header(&self, fname: &str, file: &mut File) -> Result<()> {
        println!("[+] Rules for {}:", fname);
        println!();
        println!("-----");
        print!("{}", self.comment());
        file.write_all(self.comment().as_bytes())?;
        Ok(())
    }

    fn print_fp_comment(&self, file: &mut File) -> Result<()> {
        let fp_text = "# Fingerprints:\n";
        print!("{}", fp_text);
        file.write_all(fp_text.as_bytes())?;
        Ok(())
    }

    fn print_footer(&self) {
        println!("-----");
        println!();
    }

    fn open_file<P: AsRef<Path>>(&self, fpath: P) -> Result<File, Error> {
        let parent = fpath.as_ref().parent().ok_or_else(|| {
            Error::WrongParent(fpath.as_ref().to_str().unwrap().to_string())
        })?;
        // Create the directory if does not exists
        fs::create_dir_all(parent)?;
        // Create the file if it does not exists
        let file = OpenOptions::new().create(true).append(true).open(fpath)?;
        Ok(file)
    }

    fn print_rules<F>(
        &self,
        prefix: &str,
        file: &mut File,
        fmt_fn: F,
        relays: &[tor_netdir::Relay<'_>],
    ) -> Result<(), anyhow::Error>
    where
        F: Fn(&str, &tor_netdir::Relay<'_>) -> String,
    {
        for relay in relays {
            let rule = fmt_fn(prefix, relay);
            print!("{}", rule);
            file.write_all(rule.as_bytes())?;
        }
        Ok(())
    }

    /// Generate lines to be appended to `APPROVED_ROUTERS_PATH`
    ///
    /// Depending on the subcmds:
    /// - `reject`: will write lines `!reject <fp>`
    /// - `bad-exit`: will write lines `!badexit <fp>`
    /// - `middleonly`: will write lines `!middleonly <fp>`
    /// - `reject-bad`: will write lines `!reject <fp>` and it will also
    ///    write to `BAD_PATH` lines as:
    ///
    ///    ````
    ///    # Fingerprints:
    ///    #               2BC31B73E0000B66981F7734D2B1F2C16C27D0BB
    ///    #               5BE999DDB0916332AC21CE4AE9CED29FD7AAB284
    ///    #               94A8976E00C68ED23695D0668D87B3E7F126AF62
    ///    AuthDirReject 65.109.16.131
    ///    AuthDirReject 104.200.30.152
    ///    AuthDirReject 2600:3c03::f03c:93ff:fecc:2d20
    ///    AuthDirReject 155.248.213.203
    ///    ```
    ///
    /// If one of `RelayAttr` is `ff` or `fp` it'll print all the fingeprints,
    /// even if those relays are not found in the consensus.
    ///
    fn append_outcfg(
        &self,
        netdir: &tor_netdir::NetDir,
        tokens: &'static (&str, &str),
        root_path: &Path,
    ) -> Result<()> {
        let relays =
            queries::QueryExpr::new(&self.query_args).select_relays(netdir);
        self.append_outcfg_from_relays(relays, tokens, root_path)?;
        let unusable_relays = queries::QueryExpr::new(&self.query_args)
            .select_unusable_relays(netdir);
        if !unusable_relays.is_empty() {
            println!(
                "[+] Found {} unusable relays matching: {:?}",
                unusable_relays.len(),
                self.query_args
            );
            println!("They are included in the output config files.");
        };
        Ok(())
    }

    fn append_outcfg_from_relays<P: AsRef<Path>>(
        &self,
        relays: Vec<tor_netdir::Relay<'_>>,
        tokens: &'static (&str, &str),
        root_path: P,
    ) -> Result<()> {
        let all_fps = self.all_fps();
        if !all_fps.is_empty() || !relays.is_empty() {
            // Write into `APPROVED_ROUTERS_PATH` in all the cases
            let fpath = root_path.as_ref().join(APPROVED_ROUTERS_PATH);
            let mut file = self.open_file(&fpath)?;
            self.print_header(fpath.to_str().unwrap(), &mut file)?;

            // Append all the fingerprints if they are given
            if !all_fps.is_empty() {
                for fp in &all_fps {
                    let rule = fmt_fp_str_rule(tokens.1, fp.as_str());
                    print!("{}", rule);
                    file.write_all(rule.as_bytes())?;
                }
            // Otherwise print only the relays found.
            } else {
                self.print_rules(tokens.1, &mut file, fmt_fp_rule, &relays)?;
            }

            self.print_footer();
        }

        // Create bad.conf config only when `tokens.0` has a value
        // (`AuthDirReject`).
        // We can only add here the relays for which know the fp and the address.
        if !tokens.0.is_empty() && (!relays.is_empty()) {
            // When token.0 is present, write also into BAD_PATH
            let fpath = root_path.as_ref().join(BAD_PATH);
            let mut file = self.open_file(&fpath)?;
            self.print_header(fpath.to_str().unwrap(), &mut file)?;

            // Print also the fingeprints in a comment
            self.print_fp_comment(&mut file)?;
            self.print_rules(
                "#              ",
                &mut file,
                fmt_fp_rule,
                &relays,
            )?;
            // Print the addresses when there's the `AuthDirReject` token
            self.print_rules(tokens.0, &mut file, fmt_addr_rule, &relays)?;
            self.print_footer();
        }

        println!(
            "[+] Found {} relays matching: {:?}",
            relays.len(),
            self.query_args
        );
        println!(
            "    Found relays: {:?}",
            relays
                .iter()
                .map(|r| r
                    .rsa_id()
                    .to_string()
                    .replace('$', "")
                    .to_uppercase())
                .collect::<Vec<_>>()
        );
        Ok(())
    }

    /// Return the relays' fingerprints in the `FpsFileFiler` query_arg.
    /// Since `self` doesn't have a vector with the parsed fingeprints from
    /// the file, it is needed to parse the query_args to obtain them.
    ///
    fn all_fps(&self) -> Vec<String> {
        let mut fps = Vec::new();
        for query_arg in &self.query_args {
            if let queries::RelayAttr::FpsFileQuery(relay_fingerprints) =
                &query_arg.relay_attr
            {
                for relay_fingerprint in relay_fingerprints {
                    fps.push(relay_fingerprint.to_string())
                }
            }
            if let queries::RelayAttr::Fingerprint(relay_fingerprint) =
                &query_arg.relay_attr
            {
                fps.push(relay_fingerprint.rsa_hex_str().unwrap().to_string())
            }
        }
        fps.sort();
        fps
    }
}

#[async_trait]
impl RunnableOffline for OutCfgCmd {
    fn run(&self, netdir: &tor_netdir::NetDir) -> Result<()> {
        let exe = &env::current_exe()?;
        let root_path_str =
            exe.parent().expect("Executable not in a directory.");
        let root_path = Path::new(root_path_str);
        match &self.subcmd {
            OutCfgSubCmd::BadExit(r) => {
                r.append_outcfg(netdir, &BADEXIT_TOKENS, root_path)
            }
            OutCfgSubCmd::Reject(r) => {
                r.append_outcfg(netdir, &REJECT_TOKENS, root_path)
            }
            OutCfgSubCmd::RejectBad(r) => {
                r.append_outcfg(netdir, &REJECTBAD_TOKENS, root_path)
            }
            OutCfgSubCmd::MiddleOnly(r) => {
                r.append_outcfg(netdir, &MIDDLEONLY_TOKENS, root_path)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::cli::util;
    use std::env::temp_dir;

    #[test]
    fn open_file() {
        let binding = temp_dir().join("approved-routers.conf");
        let fname = binding.to_str().unwrap();
        let subcmd_args = OutCfgSubCmdArgs {
            added_by: ADDED_BY.to_string(),
            ticket: 1,
            query_args: Vec::from([queries::QueryArg::new(
                false,
                queries::RelayAttr::Nickname("moria1".to_string()),
            )]),
            reason: None,
            project: "bad-relays-report".to_string(),
        };
        let file = subcmd_args.open_file(fname);
        assert!(file.is_ok());

        let fname = "/root";
        let file = subcmd_args.open_file(fname);
        assert!(file.is_err());
    }

    #[test]
    fn print_header_ok() {
        let binding = temp_dir().join("approved-routers.conf");
        let fname = binding.to_str().unwrap();
        let subcmd_args = OutCfgSubCmdArgs {
            added_by: ADDED_BY.to_string(),
            ticket: 1,
            query_args: Vec::from([queries::QueryArg::new(
                false,
                queries::RelayAttr::Nickname("moria1".to_string()),
            )]),
            reason: None,
            project: "bad-relays-report".to_string(),
        };
        let mut file = subcmd_args.open_file(fname).unwrap();

        let result = subcmd_args.print_header(fname, &mut file);
        assert!(result.is_ok());
    }

    #[test]
    fn all_fps_ok() {
        let path =
            Path::new(env!("CARGO_MANIFEST_DIR")).join("testdata/fps.txt");
        let subcmd_args = OutCfgSubCmdArgs {
            added_by: ADDED_BY.to_string(),
            ticket: 1,
            query_args: Vec::from([
                queries::QueryArg::new(
                    false,
                    queries::RelayAttr::FpsFileQuery(
                        util::fpfile2fps(&path).unwrap(),
                    ),
                ),
                queries::QueryArg::new(false, queries::RelayAttr::Port(444)),
            ]),
            reason: None,
            project: "bad-relays-report".to_string(),
        };
        let expected_all_fps = [
            "0011BD2485AD45D984EC4159C88FC066E5E3300E".to_string(),
            "0123456789abcdef0123456789abcdef01234567"
                .to_string()
                .to_uppercase(),
        ];
        let all_fps = subcmd_args.all_fps();
        let matching = expected_all_fps
            .iter()
            .zip(&all_fps)
            .filter(|&(a, b)| a == b)
            .count();
        assert!(matching == 2);
    }

    #[test]
    fn rejectbad_fps_no_relays() {
        let path =
            Path::new(env!("CARGO_MANIFEST_DIR")).join("testdata/fps.txt");
        let subcmd_args = OutCfgSubCmdArgs {
            added_by: ADDED_BY.to_string(),
            ticket: 1,
            query_args: Vec::from([queries::QueryArg::new(
                false,
                queries::RelayAttr::FpsFileQuery(
                    util::fpfile2fps(&path).unwrap(),
                ),
            )]),
            reason: None,
            project: "bad-relays-report".to_string(),
        };
        // let tempdir = tempfile::tempdir().unwrap();
        let root_path = temp_dir();
        println!("{}", root_path.display());
        // Assumning none of the fingeprints are found in the consensus
        subcmd_args
            .append_outcfg_from_relays(vec![], &REJECTBAD_TOKENS, &root_path)
            .unwrap();

        let fpath = root_path.join(APPROVED_ROUTERS_PATH);
        let data = fs::read_to_string(&fpath).unwrap();
        // Take the last 3 lines (last one is a `\n`)
        let mut reject_lines =
            data.split('\n').rev().take(3).collect::<Vec<_>>();
        reject_lines.reverse();
        // Join them with '\n' and add back the first '\n'
        let mut reject_string = reject_lines.join("\n");
        reject_string.insert(0, '\n');
        let expected_data = concat!(
            "\n!reject 0011BD2485AD45D984EC4159C88FC066E5E3300E",
            "\n!reject 0123456789ABCDEF0123456789ABCDEF01234567\n",
        )
        .to_string();
        assert_eq!(expected_data, reject_string);
        fs::remove_file(fpath).unwrap();
        fs::remove_dir(root_path.join("approved-routers.d")).unwrap();

        let fpath = root_path.join(BAD_PATH);
        let data = fs::read_to_string(fpath);
        assert!(data.is_err());
    }

    #[test]
    fn reason_from_str() {
        let r = Reason::from_str("1").unwrap();
        assert_eq!(Reason::ExitTrafficModification, r);
        assert_eq!("Exit modified traffic.", r.to_string());
        let r = Reason::from_str("6").unwrap();
        assert_eq!(Reason::Other("6".to_string()), r);
        assert_eq!("6", r.to_string());
        let r = Reason::from_str("foo bar").unwrap();
        assert_eq!(Reason::Other("foo bar".to_string()), r);
        assert_eq!("foo bar", r.to_string());
    }
}
