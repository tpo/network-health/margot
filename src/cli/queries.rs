use anyhow::Result;
use clap::Args;
use ipnetwork::IpNetwork;
use std::fmt;
use std::ops::Deref;
use std::path::Path;
use std::str::FromStr;

use crate::cli::err::Error;
use crate::cli::util;

use tor_geoip::GeoipDb;
use tor_linkspec::HasRelayIds;
use tor_llcrypto::pk::rsa::RsaIdentity;
use tor_netdoc::doc::netstatus;
use tor_netdoc::types::policy::PortPolicy;

static DB_V4_ASN: &str = include_str!("../../data/geoip-plus-asn");
static DB_V6_ASN: &str = include_str!("../../data/geoip6-plus-asn");

// Can't use `PartialEq` cause of netstatus::RelayFlags
#[derive(Debug, Clone)]

/// Relays' attributes that they must (not) match
///
/// Eg: `FpsFileQuery([Rsa("0123456789abcdef0123456789abcdef01234567"), ...])`
///
pub enum RelayAttr {
    /// Address or Network
    Address(IpNetwork),
    /// Relay fingerprint
    Fingerprint(util::RelayFingerprint),
    /// Relay flags,
    Flags(netstatus::RelayFlags),
    /// Relay nickname
    Nickname(String),
    /// ORPort
    Port(u16),
    /// Relay version
    Version(String),
    /// Port policy
    PortPolicyAttr(PortPolicy),
    FpsFileQuery(Vec<util::RelayFingerprint>),
    Asn(u32),
}

#[derive(Debug, Clone)]
pub struct QueryArg {
    exclude: bool,
    pub relay_attr: RelayAttr,
    /// tor's geoip database with ASN information, for `RelayAttr::Asn`
    geoipdb: Option<GeoipDb>,
}

impl QueryArg {
    pub fn new(n: bool, f: RelayAttr) -> Self {
        Self {
            exclude: n,
            relay_attr: f.clone(),
            geoipdb: if matches!(f, RelayAttr::Asn(_)) {
                Some(
                    GeoipDb::new_from_legacy_format(DB_V4_ASN, DB_V6_ASN)
                        .expect("Error creating geoip db."),
                )
            } else {
                None
            },
        }
    }

    pub fn match_relay(&self, relay: &tor_netdir::Relay) -> bool {
        let mut ret = match &self.relay_attr {
            RelayAttr::Address(a) => {
                relay.rs().orport_addrs().any(|addr| a.contains(addr.ip()))
            }
            RelayAttr::Nickname(n) => relay.rs().nickname().contains(n),
            RelayAttr::Fingerprint(fp) => fp.match_relay(relay),
            RelayAttr::Flags(f) => relay.rs().flags().contains(*f),
            RelayAttr::Port(p) => {
                relay.rs().orport_addrs().any(|addr| addr.port() == *p)
            }
            RelayAttr::Version(v) => relay
                .rs()
                .version()
                .as_ref()
                .expect("version error")
                .to_string()
                .contains(v),
            RelayAttr::PortPolicyAttr(pp) => {
                relay.low_level_details().ipv4_policy().deref() == pp
            }
            // ^ this is `&Arc<PortPolicy>`, 1st dereference `Arc`,
            // then `&`, then add `&` to match `&PortPolicy`
            // The following will try to find the relay in the list of parsed
            // fingerprints from a file.
            RelayAttr::FpsFileQuery(ff) => {
                ff.iter().any(|fp| fp.match_relay(relay))
            }
            RelayAttr::Asn(asn) => {
                let asns = util::ips2asn(
                    self.geoipdb.as_ref().unwrap(),
                    relay.rs().orport_addrs().map(|s| s.ip()).collect(),
                );
                asns.contains(asn)
            }
        };
        ret ^= self.exclude;
        ret
    }

    /// Obtain the `RsaIdentity` from a `relay_attr`, if possible
    pub fn rsa_id(&self) -> Option<RsaIdentity> {
        if let RelayAttr::Fingerprint(fp) = &self.relay_attr {
            return fp.rsa_id();
        }
        None
    }

    /// Select `unusable` (without microdescriptor) relay
    ///
    /// An unusable relay can only be found if `relay_attr` is an rsa.
    ///
    pub fn select_unusable_relay<'a>(
        &self,
        netdir: &'a tor_netdir::NetDir,
    ) -> Option<tor_netdir::UncheckedRelay<'a>> {
        if let Some(rsa_id) = self.rsa_id() {
            if let Some(unchecked_relay) = netdir.by_rsa_id_unchecked(&rsa_id)
            {
                if !unchecked_relay.is_usable() {
                    return Some(unchecked_relay);
                };
            }
        }
        None
    }
}

#[derive(Args, Debug, Clone)]
pub struct QueryExpr {
    #[arg(short, long)]
    pub oneline: bool,
    /// The query_args of the query_expr command.
    query_args: Vec<QueryArg>,
}

impl QueryExpr {
    pub fn new(query_args: &[QueryArg]) -> Self {
        QueryExpr {
            oneline: false,
            query_args: query_args.to_vec(),
        }
    }

    pub fn match_relay(&self, relay: &tor_netdir::Relay) -> bool {
        for query_arg in &self.query_args {
            if !query_arg.match_relay(relay) {
                return false;
            }
        }
        true
    }

    /// Select `unusable` (without microdescriptor) relays
    pub fn select_unusable_relays<'a>(
        &self,
        netdir: &'a tor_netdir::NetDir,
    ) -> Vec<tor_netdir::UncheckedRelay<'a>> {
        let mut relays: Vec<_> = self
            .query_args
            .iter()
            .filter_map(|qa| qa.select_unusable_relay(netdir))
            .collect();
        relays.sort_by(|a, b| b.rsa_identity().cmp(&a.rsa_identity()));
        relays
    }

    pub fn select_relays<'a>(
        &self,
        netdir: &'a tor_netdir::NetDir,
    ) -> Vec<tor_netdir::Relay<'a>> {
        let mut relays: Vec<_> =
            netdir.relays().filter(|r| self.match_relay(r)).collect();
        relays.sort_by(|a, b| b.rsa_id().cmp(a.rsa_id()));
        // unwrap_or(default: None)
        relays
    }

    pub fn count(&self, netdir: &tor_netdir::NetDir) -> usize {
        netdir.relays().filter(|r| self.match_relay(r)).count()
    }
}

impl fmt::Display for QueryExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.query_args)
    }
}

impl FromStr for QueryArg {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let exclude = s.contains("-:");
        if let Some(kv) = s.to_string().replace("-:", "").split_once(':') {
            let relay_attr = match kv.0 {
                "a" | "addr" => RelayAttr::Address(kv.1.parse().unwrap()),
                "fl" | "flag" => {
                    RelayAttr::Flags(util::parse_routerflag(kv.1))
                }
                "f" | "fp" => RelayAttr::Fingerprint(
                    kv.1.parse::<util::RelayFingerprint>()?,
                ),
                "n" | "nick" => RelayAttr::Nickname(String::from(kv.1)),
                "p" | "port" => RelayAttr::Port(kv.1.parse().unwrap()),
                "v" | "version" => RelayAttr::Version(String::from(kv.1)),
                // Because a [PortPolicy] already contains `accept` or
                // `reject`, there is no need to use the `exclude` argument
                // (`-`). If used, it will still negate the policy, ie.
                // `pp-:"accept 25"` has the same effect as `pp:"reject 25".`
                "pp" | "portpolicyfilter" => {
                    RelayAttr::PortPolicyAttr(kv.1.parse::<PortPolicy>()?)
                }
                // It takes the port policy from a file and expect the ports or
                // port ranges to be separated by spaces or commas.
                // Example: `pf:policy_accept.txt pf:policy_reject.txt`
                "pf" | "portpolicyfile" => RelayAttr::PortPolicyAttr(
                    util::portpolicyfile2portpolicy(Path::new(kv.1))?,
                ),
                "ff" | "fingerprintfile" => {
                    RelayAttr::FpsFileQuery(util::fpfile2fps(Path::new(kv.1))?)
                }
                "as" | "asn" => RelayAttr::Asn(kv.1.parse().unwrap()),
                _ => return Err(Error::UnrecognizedFilter(kv.0.to_string())),
            };
            return Ok(QueryArg::new(exclude, relay_attr));
        }
        Err(Error::InvalidFilter(s.to_string()))
    }
}

/// Write `exclude` and `relay_attr` but not `db`
impl fmt::Display for QueryArg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "exclude: {}, relay_attr: {:?}",
            self.exclude, self.relay_attr
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use util::RelayFingerprint;

    #[test]
    fn port_policy_attr_from_str() {
        let query_arg_str = "pp:accept 20-23,43,53,79-81,88,110,143,194,220";
        let query_arg = QueryArg::from_str(query_arg_str).unwrap();
        let port_policy_attr = query_arg.relay_attr;
        matches!(port_policy_attr, RelayAttr::PortPolicyAttr(_));
    }

    #[test]
    #[should_panic(expected = "WrongPolicy(InvalidPolicy)")]
    // Ports are not in order, what happens when `-` is removed from `20-23`.
    fn port_policy_attr_from_str_invalid_policy() {
        let query_arg_str = "pp:accept 2023,43,53,79-81,88,110,143,194,220";
        let query_arg = QueryArg::from_str(query_arg_str).unwrap();
        let port_policy_attr = query_arg.relay_attr;
        matches!(port_policy_attr, RelayAttr::PortPolicyAttr(_));
    }

    #[test]
    fn rsa_id_from_relay_attr() {
        let fp = "1A25C6358DB91342AA51720A5038B72742732498";
        let relay_fingerprint = RelayFingerprint::from_str(fp).unwrap();
        let relay_attr = RelayAttr::Fingerprint(relay_fingerprint);
        let query_arg = QueryArg::new(false, relay_attr);
        let expected_rsa_id = RsaIdentity::from_hex(fp);
        let rsa_id = query_arg.rsa_id();
        assert_eq!(expected_rsa_id, rsa_id);
    }
}
