use anyhow::Result;
use async_trait::async_trait;
use clap::Parser;
use std::collections::BTreeMap;
use std::fmt;

use tor_netdoc::doc::netstatus;

// use crate::commands::err::Error;
use crate::cli::util;
use crate::runnable::RunnableOffline;

#[derive(Parser, Debug)]
pub struct VersionsCmd {}

pub fn print_header() {
    println!("\n== Output format ==");
    println!("count: version   (MAJOR version) percent weight %");
}

pub fn print_totals(relays_sum: u32, weight_perc_sum: f32, weight_sum: u64) {
    println!("=== Sums ===");
    let sum = weight_sum / 1000000;
    println!("{relays_sum:>5}:        {weight_perc_sum:>25.2} % ({sum} GB/s)");
}

pub fn weight_perc(weight: u64, total_weight: u64) -> f32 {
    weight as f32 / total_weight as f32 * 100.0
}

#[derive(PartialEq, Debug)]
pub struct VersionInfo {
    count: u32,
    weight_sum: u64,
    weight_perc: f32,
}

impl VersionInfo {
    fn create(weight: u64) -> VersionInfo {
        VersionInfo {
            count: 1,
            weight_sum: weight,
            weight_perc: 0.0,
        }
    }

    fn update(&mut self, weight: u64) {
        self.count += 1;
        self.weight_sum += weight;
    }

    fn update_weight_perc(&mut self, total_weight: u64) {
        self.weight_perc = weight_perc(self.weight_sum, total_weight);
    }
}

#[derive(PartialEq, Debug)]
pub struct VersionsInfo(BTreeMap<String, VersionInfo>);

impl fmt::Display for VersionsInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (version, version_info) in &self.0 {
            let mut major = "       ";
            if util::is_major_version((&version).to_string()) {
                major = "[MAJOR]";
            }
            writeln!(
                f,
                "{:>5}: {version:<18}{major} {:.2} %",
                version_info.count, version_info.weight_perc,
            )?;
        }
        Ok(())
    }
}

impl VersionsInfo {
    pub fn new() -> VersionsInfo {
        VersionsInfo(BTreeMap::new())
    }

    pub fn create_update_version_info(
        &mut self,
        version: &String,
        weight: u64,
    ) {
        match self.0.get_mut(version) {
            Some(version_info) => version_info.update(weight),
            _ => {
                let version_info = VersionInfo::create(weight);
                self.0.insert(version.clone(), version_info);
            }
        };
    }

    pub fn update_perc(&mut self, total_weight: u64) {
        for version_info in self.0.values_mut() {
            version_info.update_weight_perc(total_weight)
        }
    }

    pub fn create(
        &mut self,
        relays: &[&tor_netdir::Relay],
    ) -> &mut VersionsInfo {
        // Create new BTreeMap, even if it already existed.
        self.0 = BTreeMap::new();
        let mut total_weight: u64 = 0;

        for relay in relays {
            let weight = util::weight2u64(relay);
            let version = relay.rs().version().expect("").to_string();
            total_weight += weight;

            self.create_update_version_info(&version, weight);
            let major_version = version[..5].to_string();
            self.create_update_version_info(&major_version, weight);
            self.update_perc(total_weight);
        }
        self
    }

    pub fn totals(&self) -> (u32, f32, u64) {
        let mut relays_sum: u32 = 0;
        let mut weight_perc_sum: f32 = 0.0;
        let mut weight_sum: u64 = 0;
        for (version, version_info) in &self.0 {
            if util::is_major_version((&version).to_string()) {
                continue;
            }
            relays_sum += version_info.count;
            weight_perc_sum += version_info.weight_perc;
            weight_sum += version_info.weight_sum;
        }
        (relays_sum, weight_perc_sum, weight_sum)
    }

    fn generate(
        &mut self,
        running_relays: &[tor_netdir::Relay<'_>],
        exit: bool,
    ) {
        let relays: Vec<&tor_netdir::Relay> = running_relays
            .iter()
            .filter(|r| if exit { r.rs().is_flagged_exit() } else { true })
            .collect();
        self.create(&relays);
        let (relays_sum, weight_perc_sum, weight_sum) = self.totals();

        if !exit {
            println!("\n== Relays (including exits) ==")
        } else {
            println!("\n== Only exits ==")
        };
        println!("{}", self);
        print_totals(relays_sum, weight_perc_sum, weight_sum);
    }

    pub fn generate_all(&mut self, netdir: &tor_netdir::NetDir) {
        let running_relays: Vec<tor_netdir::Relay> = netdir
            .relays()
            .filter(|r| {
                (*r).rs().flags().contains(netstatus::RelayFlags::RUNNING)
            })
            .collect();
        println!("\t- {} running relays in consensus", running_relays.len());
        print_header();

        self.generate(&running_relays, false);
        self.generate(&running_relays, true);
    }
}

#[async_trait]
impl RunnableOffline for VersionsCmd {
    fn run(&self, netdir: &tor_netdir::NetDir) -> Result<()> {
        println!("[+] Getting relays from consensus");
        let mut versions_info = VersionsInfo::new();
        versions_info.generate_all(netdir);
        Ok(())
    }
}
