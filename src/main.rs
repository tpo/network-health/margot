mod cli;
// mod query_args;
mod runnable;

use anyhow::Result;
use clap::Parser;
use std::sync::Arc;
use tracing_subscriber::{filter, prelude::*};

use crate::cli::Cli;
use crate::runnable::Runnable;
use arti_client::{TorClient, TorClientConfig};
use tokio_crate as tokio;
// use tor_rtcompat::{Runtime};

#[macro_use]
extern crate prettytable;

#[tokio::main]
async fn main() -> Result<()> {
    let cli = Cli::parse();
    // Set logging level from cli option
    let filter = cli.log_level.parse::<filter::Targets>()?;
    // Build a new subscriber with the `fmt` layer using the `Targets`
    // filter constructed above.
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();

    let config = TorClientConfig::default();
    let arti_client = Arc::new(TorClient::create_bootstrapped(config).await?);
    cli.subcmd.run(&arti_client).await
}
