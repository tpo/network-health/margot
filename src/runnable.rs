use anyhow::Result;
use async_trait::async_trait;
use tor_rtcompat::Runtime;

#[async_trait]
pub trait Runnable<R: Runtime> {
    async fn run(&self, arti_client: &arti_client::TorClient<R>)
        -> Result<()>;
}

pub trait RunnableOffline {
    fn run(&self, netdir: &tor_netdir::NetDir) -> Result<()>;
}

#[async_trait]
impl<T: RunnableOffline + Send + Sync, R: Runtime> Runnable<R> for T {
    async fn run(
        &self,
        arti_client: &arti_client::TorClient<R>,
    ) -> Result<()> {
        let netdir = arti_client.dirmgr().timely_netdir()?;
        self.run(&netdir)
    }
}
